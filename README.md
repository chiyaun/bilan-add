# 碧蓝航线立绘合成小工具

## 介绍
碧蓝航线立绘合成小工具

## 软件架构
PyQt5


## 安装教程

pip install -r requirements.txt

#### 使用说明

python环境下直接，运行main.py

# 简介

本软件是基于pyqt的一个小工具，由于是python的原因，打包后安装包过大，不必在意。

下载后，解压，打开[BlueLineDrawingSynthesis.exe](output%2FBlueLineDrawingSynthesis%2FBlueLineDrawingSynthesis.exe)碧蓝航线立绘合成小工具.exe即可。

![img.png](./docx/img.png)