# coding: utf-8
import sys

from qfluentwidgets import QConfig, FolderValidator, ConfigItem, isDarkTheme, MSFluentTitleBar


def isWin11():
    return sys.platform == 'win32' and sys.getwindowsversion().build >= 22000


if isWin11():
    from qframelesswindow import AcrylicWindow as Window
else:
    from qframelesswindow import FramelessWindow as Window


class MicaWindow(Window):

    def __init__(self):
        super().__init__()
        self.setTitleBar(MSFluentTitleBar(self))
        if isWin11():
            self.windowEffect.setMicaEffect(self.winId(), isDarkTheme())


class Config(QConfig):
    OpenDir = ConfigItem("Folders", "OpenDir", "./", FolderValidator())


cfg = Config()
