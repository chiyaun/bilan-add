from pathlib import Path

from PyQt5.QtWidgets import QFileDialog, QLabel, QApplication
from qfluentwidgets import InfoBar

from .conf import cfg, MicaWindow
from .myThread import CraftThread
from .ui_base import UI_MainWindow, MaskDialog


class MainWindow(MicaWindow, UI_MainWindow):
    def __init__(self):
        super().__init__()
        self.backgroundLabel = QLabel(self)
        self.mask_dialog = MaskDialog(self)
        self.craftThread = CraftThread(self)
        self.__initWidgets()
        self.__initSignals()

    def __initWidgets(self):
        self.setMinimumSize(1000, 700)
        self.__initWindowCenter()
        self.setUp_UI(self)
        self.mask_dialog.raise_()
        self.mask_dialog.hide()
        self.backgroundLabel.move(0, 0)
        self.backgroundLabel.resize(self.width(), self.height())
        self.backgroundLabel.lower()
        self.backgroundLabel.setStyleSheet(
            '''
             background-color: transparent;
             image: url(background.png);
             image-position: right;
             image-clip: content-box;
            '''
        )

    def __initSignals(self):
        self.craftThread.started.connect(self.mask_dialog.show)
        self.craftThread.signal.connect(self.mask_dialog.changeValue)
        self.craftThread.finished.connect(lambda: self.mask_dialog.pushButton.setText('关闭'))

    def __initWindowCenter(self):
        desktop = QApplication.desktop().availableGeometry()
        w, h = desktop.width(), desktop.height()
        self.move(w // 2 - self.width() // 2, h // 2 - self.height() // 2)

    def open_dir(self, text: str):
        file_dir = QFileDialog.getExistingDirectory(self, '打开文件夹', cfg.get(cfg.OpenDir))
        if text == 'Mesh':
            self.meshLineEdit.setText(file_dir)
        elif text == 'Texture2D':
            self.textureLineEdit.setText(file_dir)
        elif text == 'Save':
            self.saveLineEdit.setText(file_dir)
        cfg.set(cfg.OpenDir, file_dir, True)

    def start_merge(self):
        """
        开始合并
        :return:
        """
        meshText = self.meshLineEdit.text()
        textTureText = self.textureLineEdit.text()
        saveText = self.saveLineEdit.text()

        if Path(meshText).is_dir() is False or len(meshText) <= 1:
            self.messagePrompt('warning')
            return
        if Path(textTureText).is_dir() is False or len(textTureText) <= 1:
            self.messagePrompt('warning')
            return
        if Path(saveText).is_dir() is False or len(saveText) <= 1:
            self.messagePrompt('warning')
            return
        self.messagePrompt('success')
        self.craftThread.setValues(meshText, textTureText, saveText)

    def messagePrompt(self, s: str):
        if s == 'warning':
            InfoBar.warning(
                title='警告！',
                content="路径错误，请重新检查路径!",
                isClosable=True,  # disable close button
                duration=2000,
                parent=self.window()
            )
        elif s == 'success':
            InfoBar.success(
                title='提示',
                content="路径正确，开始提取合并",
                isClosable=True,
                duration=2000,
                parent=self.window()
            )

    def resizeEvent(self, e):
        super().resizeEvent(e)
        self.backgroundLabel.resize(e.size())
