from PyQt5.QtCore import Qt
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import (QVBoxLayout, QWidget, QHBoxLayout, QTextBrowser, QSpacerItem, QSizePolicy)

from qfluentwidgets import (ProgressBar, PushButton, FluentIcon, LineEdit, BodyLabel, TitleLabel, setFont)
from qfluentwidgets.components.dialog_box.mask_dialog_base import MaskDialogBase


class MaskDialog(MaskDialogBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.verticalLayout = QVBoxLayout(self.widget)
        self.bodyLabel = BodyLabel(self.widget)
        self.progressBar = ProgressBar(self.widget)
        self.textEdit = QTextBrowser(self.widget)
        self.pushButton = PushButton('取消', self.widget)

        self.bodyLabel.setText('共0个文件，已合并0个文件')
        setFont(self.textEdit)
        self.progressBar.setValue(0)
        self.pushButton.clicked.connect(self.close)
        self._hBoxLayout.setContentsMargins(100, 100, 100, 100)
        self.verticalLayout.setContentsMargins(50, 50, 50, 50)
        self.verticalLayout.addWidget(self.bodyLabel, 0, Qt.AlignTop)
        self.verticalLayout.addWidget(self.progressBar)
        self.verticalLayout.addWidget(self.textEdit)
        self.verticalLayout.addWidget(self.pushButton, 0, Qt.AlignBottom)
        self.textEdit.setStyleSheet('background-color: transparent; border: none;')
        self.widget.setStyleSheet('background-color: white; border-radius: 10px;')

    def changeValue(self, lt: str, s: str, i: int):
        self.bodyLabel.setText(lt)
        self.progressBar.setValue(i)

        text_cursor = QTextCursor(self.textEdit.textCursor())
        text_cursor.setPosition(0, QTextCursor.MoveAnchor)  # 移动光标
        text_cursor.insertHtml('<p style="font-size:14px;color:red;">{}</p>'.format(s))
        text_cursor.insertHtml('<br>')
        self.textEdit.setTextCursor(text_cursor)


class UI_MainWindow(object):
    def setUp_UI(self, widget: QWidget):
        self.VerticalLayout = QVBoxLayout(widget)
        self.titleLabel = TitleLabel('碧蓝航线立绘合成小工具', widget)

        self.meshHorizontalLayout = QHBoxLayout(widget)
        self.meshLineEdit = LineEdit(widget)
        self.meshLineEdit.setPlaceholderText('Mesh路径')
        self.meshButton = PushButton('选择文件夹', self, FluentIcon.FOLDER)
        self.meshButton.clicked.connect(lambda: widget.open_dir('Mesh'))
        self.meshHorizontalLayout.addWidget(self.meshLineEdit)
        self.meshHorizontalLayout.addWidget(self.meshButton)

        self.textureHorizontalLayout = QHBoxLayout(widget)
        self.textureLineEdit = LineEdit(widget)
        self.textureLineEdit.setPlaceholderText('Texture2D路径')
        self.textureButton = PushButton('选择文件夹', widget, FluentIcon.FOLDER)
        self.textureButton.clicked.connect(lambda: widget.open_dir('Texture2D'))
        self.textureHorizontalLayout.addWidget(self.textureLineEdit)
        self.textureHorizontalLayout.addWidget(self.textureButton)

        self.saveHorizontalLayout = QHBoxLayout(widget)
        self.saveLineEdit = LineEdit(widget)
        self.saveLineEdit.setPlaceholderText('合成文件保存路径')
        self.saveButton = PushButton('选择文件夹', self, FluentIcon.FOLDER)
        self.saveButton.clicked.connect(lambda: widget.open_dir('Save'))
        self.saveHorizontalLayout.addWidget(self.saveLineEdit)
        self.saveHorizontalLayout.addWidget(self.saveButton)

        self.startButton = PushButton('开始合成', widget)
        self.startButton.clicked.connect(widget.start_merge)
        self.label = BodyLabel(widget)
        self.label.setText('注：本程序只支持painting文件夹中的静态立绘')
        self.label.setTextColor(Qt.red, Qt.red)

        self.VerticalLayout.setContentsMargins(100, 100, 100, 100)
        self.VerticalLayout.setSpacing(20)
        self.VerticalLayout.setAlignment(Qt.AlignCenter)
        self.VerticalLayout.addStretch(1)
        self.VerticalLayout.addWidget(self.titleLabel, 0, Qt.AlignCenter)
        self.VerticalLayout.addItem(QSpacerItem(500, 50, QSizePolicy.Expanding, QSizePolicy.Fixed))
        self.VerticalLayout.addLayout(self.meshHorizontalLayout)
        self.VerticalLayout.addLayout(self.textureHorizontalLayout)
        self.VerticalLayout.addLayout(self.saveHorizontalLayout)
        self.VerticalLayout.addWidget(self.startButton)
        self.VerticalLayout.addWidget(self.label)
        self.VerticalLayout.addStretch(1)
